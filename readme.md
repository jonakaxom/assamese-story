Assamese Romantic Story 

types of love--

There are many different types of love around us all the time. They can be as weird and wonderful as the people who feel them or as innocent and pure looking as the objects that inspire them.
Here are some of the more common ones:
Acquaintance: You know each other but you have not been on a date together, for example your math teacher and your chemistry coach.
Confidant: You trust and respect someone enough to share your secrets with them.
Crush: You have a romantic interest in a person, for example Jenny Thompson has a crush on Kyle Kroot.
Friend: You have been friends with someone for so long that you've shared everything with them, or they have revealed everything about themselves to you.
Rival: You compete with someone in some way, whether it's a sports match, a schoolwork challenge or a battle of wits.
Trust: You have complete confidence that someone can be trusted to handle important things.
Lover: You are physically intimate with someone.
One: You are the only person with the person you have strong feelings for and they are the only person with you.
True: You and someone are absolutely inseparable, for example a married couple is true with each other.
Sibling: You have the same mother or father but different parents.
Stranger: You have feelings for someone you do not know.

Family: You have a very special kinship with a group of people, for example your family.
Ally: You have a group of people who you consider allies, this could be as small as just yourself and one other person or as large as an entire army.

True love is difficult to describe. The closest most of us will probably ever get is infatuation, but those are just words made up by writers. True love is something real and pure, not something that just sounds good.
Take a look at your own feelings on the matter and see if you don't agree.

The population of the world is roughly 6.8 billion people, with the United States of America being the only technologically advanced country. The vast majority of the population (including you!) lives in a few large countries.
As the world map shows, only countries that have been officially classified as having a native population are shown. Even then, some countries have dozens of different types of people living there, from dwarves to minotaurs. You are considered an "average" human, which means you would probably fall into the category of "White" on the world map. The "White" category includes the populations of most developed countries, with the notable exception of the majority of countries in Northern Europe.
The "Black" population category includes most of the inhabitants of Africa, as well as many of the Native Americans and some countries in South America.

https://www.jonakaxom.in/2018/10/Romantic-Assamese-love-story.html